<?php
/**
 * @file
 * Entity Type bundle list.
 */

module_load_include('inc', 'eck', 'eck.classes');


/**
 * Page call back for the bundle overview table.
 *
 * to see and manipulate all created label of a given type.
 *
 * @param string $entity_type
 *   The name of the entity type.
 */
function eckextend_bundle_list($entity_type) {
  $entity_type = entity_type_load($entity_type);

  // Check that the user has permissions to view bundle lists.
  // @todo Do we really need this, can they be here without the right
  // permissions in the first place?
  if (eck__multiple_access_check(array(
      'eck administer bundles',
      'eck list bundles',
      "eck administer {$entity_type->name} bundles",
      "eck list {$entity_type->name} bundles",
    )
  )) {

    $header = array(
      t('Bundle'),
      array(
        'data' => t('Operations'),
        'colspan' => '1'
      )
    );

    $rows = array();

    $bundles = Bundle::loadByEntityType($entity_type);
    usort($bundles, 'eck_alphabetical_cmp');

    foreach ($bundles as $bundle) {
      $bundle_label = $bundle->label;

      $admin_info = get_bundle_admin_info($entity_type->name, $bundle->name);

      $uri = $admin_info['path'];

      $allowed_operations = array();
      // Check that the user has permissions to delete:
      $perm_check = array(
        'eck administer bundles',
        'eck delete bundles',
        "eck administer {$entity_type->name} bundles",
        "eck delete {$entity_type->name} bundles",
      );

      if (eck__multiple_access_check($perm_check)) {
        $allowed_operations[] = l(t('edit'), "{$uri}/edit");
        $allowed_operations[] = l(t('managed properties'), "{$uri}/edit");
        $allowed_operations[] = l(t('entities'), $uri);
        $allowed_operations[] = l(t('manage fields'), "{$uri}/fields");
        $allowed_operations[] = l(t('manage display'), "{$uri}/display");
        $allowed_operations[] = l(t('delete'), "{$uri}/delete");
      }

      // Check that the user can view lists of entities.
      $perm_check = array(
        'eck administer entities',
        'eck list entities',
        "eck administer {$entity_type->name} {$bundle->name} entities",
        "eck list {$entity_type->name} {$bundle->name} entities",
      );
      if (eck__multiple_access_check($perm_check)) {
        $label = l($bundle_label, url($uri, array('absolute' => TRUE)));
      }
      else {
        $label = $bundle_label;
      }

      $rows[] = array($label, implode(' | ', $allowed_operations));
    }

    $build['bundle_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
  }

  return $build;
}
