<?php
/**
 * @file
 * Entity Type list.
 */

/**
 * Callback for the entity_type overview.
 */
function eckextend_entity_type_list() {
  $path = eck__entity_type__path();
  $header = array(
    t('Entity type'),
    array('data' => t('Operations'), 'colspan' => '1')
  );
  $rows = array();

  $entity_types = EntityType::loadAll();
  usort($entity_types, 'eck_alphabetical_cmp');

  foreach ($entity_types as $entity_type) {
    $allowed_operations = array();

    // Check that the user has permissions to delete:
    if (eck__multiple_access_check(array(
      'eck administer entity types',
      'eck delete entity types'
    ))) {
      $allowed_operations[] = l(t("bundles"), "{$path}/{$entity_type->name}");
      $allowed_operations[] = l(t("edit"), "{$path}/{$entity_type->name}/edit");
      $allowed_operations[] = l(t("properties"), "{$path}/{$entity_type->name}/properties");
      $allowed_operations[] = l(t("delete"), "{$path}/{$entity_type->name}/delete");
    }

    if (eck__multiple_access_check(array(
      'eck administer bundles',
      'eck list bundles',
      "eck administer {$entity_type->name} bundles",
      "eck list {$entity_type->name} bundles",
    ))) {
      $label = l(t("@el", array("@el" => $entity_type->label)), "{$path}/{$entity_type->name}");
    }
    else {
      $label = t("@el", array("@el" => $entity_type->label));
    }

    $rows[] = array($label, implode(' | ',$allowed_operations));
  }

  $build['entity_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $build;
}
