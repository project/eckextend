<?php

/**
 * @file
 * Implementation of 'Number (varchar)' property behavior plugin for eck.
 */

// Plugin definition.
$plugin = array(
  'label' => 'Number (varchar)',
  'default_widget' => 'eckextend_number_varchar_property_widget',
  // Use the module simple default_formatter.
  'default_formatter' => 'eckextend_property_default_formatter',
  // Use the default property form that implements required.
  'property_form' => 'eckextend_default_property_form',
);

/**
 * Implements 'default_widget' plugin.
 */
function eckextend_number_varchar_property_widget($property, $vars) {
  $entity = $vars['entity'];
  $value = eckextend_get_property_value($entity, $property);
  $settings = eckextend_property_settings($vars['entity']->entityType(), $property);
  return array(
    '#type' => 'textfield',
    '#title' => t('@label', array('@label' => $vars['properties'][$property]['label'])),
    '#default_value' => $value,
    '#required' => isset($settings['required']) ? $settings['required'] : FALSE,
    '#access' => eckextend_property_access($vars['entity'], $property, 'edit'),
    '#element_validate' => array('element_validate_number', 'eckextend_null_property'),
    '#maxlength' => 255,
  );
}
