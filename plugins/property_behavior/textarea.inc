<?php

/**
 * @file
 * Implementation of 'Textarea' property behavior plugin for eck.
 */

// Plugin definition.
$plugin = array(
  'label' => 'Textarea (deprecated)',
  'default_widget' => 'eckextend_textarea_property_widget',
  // Use the module simple default_formatter.
  'default_formatter' => 'eckextend_property_default_formatter',
  // Use the default property form that implements required.
  'property_form' => 'eckextend_default_property_form',
);

/**
 * Implements 'default_widget' plugin.
 */
function eckextend_textarea_property_widget($property, $vars) {
  $settings = eckextend_property_settings($vars['entity']->entityType(), $property);
  return array(
    '#type' => 'textarea',
    '#cols' => 200,
    '#rows' => 10,
    '#title' => t('@label', array('@label' => $vars['properties'][$property]['label'])),
    '#default_value' => eckextend_get_property_value($vars['entity'], $property),
    '#required' => isset($settings['required']) ? $settings['required'] : FALSE,
    '#access' => eckextend_property_access($vars['entity'], $property, 'edit'),
  );
}
